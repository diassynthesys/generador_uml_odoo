**GENERADOR DE UML**

Este generador le permite crear diagramas de clase UML a partir de las class que hereden de models.Model en los archivos models/model.py de un módulo de odoo.
Este script maneja:

- Excepciones
- Solo se le indica el nombre del módulo que debe analizar.
- Por cada models.py crea un models.dia Considerando:
     * Si la clase analizada no hereda de models.Model, no la crea.
     * Si la clase no tiene fields, la crea vacía
     * Aquellos archivos que no tengan clases heredadas de models.Model no son creados. 


- Analiza las clases de los models.py de un módulo.
- Maneja saltos de líneas en el valor de un field para colocarlo en una sola línea. Ejemplo:

     
Este código: ejem = fields.Char(string='Ejemplo',
                   help='Campo ejemplo')
                   
Esto lo coloca de la siguiente manera: ejem = fields.Char(string='Ejemplo', help='Campo ejemplo')

Para luego colocarlo en el valor de la clase.


- Los diagramas de clase pueden ser creadas de 2 formas según el usuario desee:
    1. En el mismo directorio en un carpeta llamada doc_<modulo_name> 
    2. Por convención dentro del módulo puede o no existir una carpeta llamada doc, lo cual, está opción permite crear los diagramas de clase dentro de esa carpeta.

**USO**
1. Debe tener instalado la aplicación de diagramas [dia](http://dia-installer.de/index.html.es) para visualizar el diagrama UML creado al finall.
2. Debe tener instalado python3 en su sistema operativo.
3. En la consola debe situarse al mismo nivel que el módulo que desea analizar.
4. Corra el script de la siguiente manera: python generador_uml.py
5. Ingrese el nombre del módulo que desea analizar.
6. Elija donde quiere crear los diagramas de clase.

**DONACIÓN**

Si quieres apoyar esta iniciativa puedes realizar una donación a esta cuenta [Paypal](https://www.paypal.me/HidalgoF)

Si quieres participar en el repositorio, escríbeme al correo de fhidalgo.dev@gmail.com
